<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
?>
<?php
$user = 'toto';
$pass = 'salut';
$host = '127.0.0.1';
$db   = 'design';

$pdo = new PDO("mysql:host=$host;dbname=$db;", $user, $pass);

function createProducts ($nom, $image, $prix,$son,$dispo){
    global $pdo;
    $req = $pdo->prepare("insert into produit(image,nom, prix,son,dispo) values(?, ?, ?, ?, ?);");
    $req->execute([$image, $nom, $prix, $son,$dispo]);
 };
 function readAll(){
    global $pdo;
    $req = $pdo->query("SELECT * FROM produit ;");
    return $req->fetchAll();
}
function delete($id){
    global $pdo;
    $req = $pdo->prepare("delete from produit where id=?;");
    $req->execute([$id]);
};