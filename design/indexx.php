<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>E-Commerce WebSite</title>
    <link rel="stylesheet" href="style.css" />
  </head>
  <body>
    <header>
      <div class="brand">
        <img src="images/logo.png" alt="Logo" class="Logo" />
        <h1>nom du magasin</h1>
      </div>
      <nav>
        <a href="/">Boutique</a>
        <img src="images/cart.png" class="cart" />
        <a href="panier.html">Panier (0)</a>
      </nav>
    </header>

    <main>
    <?php
    for ($i=0;$i<10;$i++){ ?>
      <div class="product">
        <img src="images/cart.png" class="product-img" />
        <div>
          <h2>Nom du produit</h2>
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Vero
            dolorum repellat officia impedit explicabo eum!
          </p>
          <div class="actions">
            <img src="images/play.png" alt="" />
            <p class="prix">150$</p>
            <button class="ajouter">Ajouter au panier</button>
          </div>
        </div>
      </div>
      <?php }?>

    </main>
  </body>
</html>
